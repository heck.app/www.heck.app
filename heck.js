function heck(app) {
	function add_menu_item(menu, item, action) {
		var entry = document.createElement("div");
		entry.appendChild(document.createTextNode(item));
		entry.onclick = action;
		menu.appendChild(entry);
	};

	var body = document.getElementsByTagName("body")[0];

	var banner = document.createElement("div");
	banner.id = "heck-banner";

	var menubutton = document.createElement("a");
	menubutton.id = "heck-menu-button";
	menubutton.onclick = function() {
		var menu = document.getElementById("heck-menu");
		if (menu.style.display == "none") {
			menu.style.display = "block";
		} else {
			menu.style.display = "none";
		}
	};
	menubutton.innerHTML = "&equiv;";

	var menu = document.createElement("div");
	menu.id = "heck-menu";
	menu.style.display = "none";
	//add_menu_item(menu, "Account", "");
	//add_menu_item(menu, "Help", "");

	banner.appendChild(menubutton);
	banner.appendChild(menu);
	banner.appendChild(document.createTextNode(app));

	body.insertBefore(banner, body.childNodes[0]);
};
